from django.urls import path, include
from app.forms import views


app_name='form'

#
urlpatterns = (
    # urls for MainForm
    path('main-form/', views.FormListView.as_view(), name='main_form_list'),
    path('main-form/create/', views.main_form_create, name='main_form_create'),
    path('main-form/update/<int:pk>/', views.main_form_update, name='main_form_update'),
    path('main-form/delete/<int:pk>/', views.main_form_delete, name='main_form_delete'),
    path('main-form/print/<int:pk>/', views.main_form_print, name='main_form_print'),

    # urls for result
    path('result/', views.ResultListView.as_view(), name='result_list'),
    path('result/create/<int:main_form_id>/', views.result_create, name='result_create'),
    path('result/update/<int:pk>/', views.result_update, name='result_update'),
    path('result/delete/<int:pk>/', views.result_delete, name='result_delete'),
)