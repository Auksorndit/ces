from django import forms
from .models import MainForm, Feedback, Result, Outcome, ResultLine

class MainFormForm(forms.ModelForm):
    class Meta:
        model = MainForm
        fields = ['name', 'detail']


class OutcomeForm(forms.ModelForm):
    class Meta:
        model = Outcome
        fields = ['name', 'main_form']


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['name', 'detail', 'score', 'outcome']


class ResultForm(forms.ModelForm):
    class Meta:
        model = Result
        fields = ['main_form']


class ResultLineForm(forms.ModelForm):
    class Meta:
        model = ResultLine
        fields = ['result', 'outcome', 'feedback']