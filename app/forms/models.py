from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User


# Create your models here.
class MainForm(models.Model):
    name = models.CharField(max_length=30)
    detail = models.CharField(max_length=50, blank=True, null=True, default=None)

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('form:main_form_list')
    
    def get_update_url(self):
        return reverse('form:main_form_update', args=(self.pk,))

    def get_delete_url(self):
        return reverse('form:main_form_delete', args=(self.pk,))
    
    def get_print_url(self):
        return reverse('form:main_form_print', args=(self.pk,))

    def __str__(self):
        return self.name


class Outcome(models.Model):
    name = models.CharField(max_length=50)
    main_form = models.ForeignKey('MainForm', related_name='+', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Feedback(models.Model):
    name = models.CharField(max_length=50)
    detail = models.CharField(max_length=100, blank=True, null=True)
    score = models.CharField(max_length=50)
    outcome = models.ForeignKey('Outcome', related_name='+', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Result(models.Model):
    main_form = models.ForeignKey('MainForm', related_name='+', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='+', on_delete=models.CASCADE, null=True, blank=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('form:result_list')
    
    def get_update_url(self):
        return reverse('form:result_update', args=(self.pk,))

    def get_delete_url(self):
        return reverse('form:result_delete', args=(self.pk,))

    def __str__(self):
        return "Result: {}".format(self.pk)


class ResultLine(models.Model):
    result = models.ForeignKey('Result', related_name='+', on_delete=models.CASCADE)
    outcome = models.ForeignKey('Outcome', related_name='+', on_delete=models.CASCADE, blank=True, null=True)
    feedback = models.ForeignKey('Feedback', related_name='+', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        ordering = ('pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def __str__(self):
        return "{}: {}({})".format(self.result.main_form, self.outcome, self.feedback.score)