from django.contrib import admin
from django import forms
from .models import MainForm, Feedback, Result, Outcome, ResultLine

# Register your models here.
class MainFormAdminForm(forms.ModelForm):
    
    class Meta:
        model = MainForm
        fields = '__all__'


class MainFormAdmin(admin.ModelAdmin):
    form = MainFormAdminForm
    list_display = ['name', 'detail']

admin.site.register(MainForm, MainFormAdmin)



class OutcomeAdminForm(forms.ModelForm):
    
    class Meta:
        model = Outcome
        fields = '__all__'


class OutcomeAdmin(admin.ModelAdmin):
    form = MainFormAdminForm
    list_display = ['name', 'main_form']

admin.site.register(Outcome, OutcomeAdmin)


class FeedbackAdminForm(forms.ModelForm):
    
    class Meta:
        model = Feedback
        fields = '__all__'


class FeedbackAdmin(admin.ModelAdmin):
    form = FeedbackAdminForm
    list_display = ['name', 'detail', 'score', 'outcome']

admin.site.register(Feedback, FeedbackAdmin)


class ResultAdminForm(forms.ModelForm):
    
    class Meta:
        model = Result
        fields = '__all__'


class ResultAdmin(admin.ModelAdmin):
    form = ResultAdminForm
    list_display = ['main_form' , 'user', 'create_at', 'update_at']

admin.site.register(Result, ResultAdmin)


class ResultLineAdminForm(forms.ModelForm):
    
    class Meta:
        model = ResultLine
        fields = '__all__'


class ResultLineAdmin(admin.ModelAdmin):
    form = ResultLineAdminForm
    list_display = ['result', 'outcome', 'feedback']

admin.site.register(ResultLine, ResultLineAdmin)
