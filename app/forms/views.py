from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic import ListView
from .forms import MainFormForm, ResultForm
from .models import MainForm, Feedback, Result, Outcome, ResultLine
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
import datetime

TEMPLATE_PATH = 'app/forms/templates/'

# Create your views here.
class FormListView(ListView):
    models = MainForm
    template_name = TEMPLATE_PATH + 'main_form_list.html'

    def get_queryset(self):
        return MainForm.objects.order_by('-pk')


def main_form_create(request):
    if request.method == 'GET':
        outcome_list = []
        outcome_feedback = {
            'outcome': [Outcome()],
            'feedback_list': [Feedback()],
        }
        outcome_list.append(outcome_feedback)
        context = {
            'outcome_list': outcome_list,
        }
        return render(request, TEMPLATE_PATH + 'main_form_form.html', context)

    elif request.method == 'POST':
        def save_feedback(request, outcome, count):
            count_feedback = int(request.POST.get('count_feedback['+str(count)+']', None))
            for i in range(0, count_feedback):
                try:
                    feedback = Feedback.objects.get(pk=int(request.POST.get('feedback_list['+str(count)+']['+str(i)+'].id', None)))
                except:
                    feedback = Feedback()
                feedback.name = request.POST.get('feedback_list['+str(count)+']['+str(i)+'].name', None)
                feedback.detail = request.POST.get('feedback_list['+str(count)+']['+str(i)+'].detail', None)
                feedback.score = request.POST.get('feedback_list['+str(count)+']['+str(i)+'].score', None)
                feedback.outcome = outcome
                feedback.save()
        
        def save_outcome(request, main_form):
            count_outcome = int(request.POST.get('count_outcome', None))
            for i in range(0, count_outcome):
                try:
                    outcome = Outcome.objects.get(pk=int(request.POST.get('outcome_list['+str(i)+'].id', None)))
                except:
                    outcome = Outcome()
                outcome.name = request.POST.get('outcome_list['+str(i)+'].name', None)
                outcome.main_form = main_form
                outcome.save()
                save_feedback(request, outcome, i)

        form = MainFormForm(request.POST)
        if form.is_valid():
            main_form = form.save(commit=False)
            if request.POST.get("id", "") != '':
                main_form_temp = MainForm.objects.get(pk=int(request.POST.get("id", "")))
                main_form.id = main_form_temp.id
            main_form.save()

            save_outcome(request, main_form)
        else:
            print(form.errors)

        return HttpResponseRedirect(reverse('form:main_form_list'))


def main_form_update(request, pk):
    if request.method == 'GET':
        main_form = MainForm.objects.get(pk=pk)
        outcome_list = []
        for outcome in Outcome.objects.filter(main_form=main_form):
            feedback_list = Feedback.objects.filter(outcome=outcome)
            outcome_feedback = {
                'outcome': outcome,
                'feedback_list': feedback_list,
            }
            outcome_list.append(outcome_feedback)
        context = {
            'main_form': main_form,
            'outcome_list': outcome_list,
        }
        return render(request, TEMPLATE_PATH + 'main_form_form.html', context)


def main_form_delete(request, pk):
    MainForm.objects.filter(pk=pk).delete()
    return HttpResponseRedirect(reverse('form:main_form_list'))


def main_form_print(request, pk):
    if request.method == 'GET':
        main_form = MainForm.objects.get(pk=pk)
        context = {
            'main_form': main_form,
        }
        return render(request, TEMPLATE_PATH + 'main_form_print.html', context)


class ResultListView(ListView):
    models = Result
    template_name = TEMPLATE_PATH + 'result_list.html'

    def get_queryset(self):
        return Result.objects.order_by('-pk')


def result_create(request, main_form_id):
    if request.method == 'GET':
        main_form_list = MainForm.objects.all()
        if main_form_id > 0:
            main_form = MainForm.objects.get(pk=int(main_form_id))
            result_line_list = []
            for outcome in Outcome.objects.filter(main_form=main_form):
                feedback_list = Feedback.objects.filter(outcome=outcome)
                outcome_feedback = {
                    'outcome': outcome,
                    'feedback_list': feedback_list,
                }
                result_line_list.append(outcome_feedback)
        else:
            main_form = None
            result_line_list = [ResultLine()]
        context = {
            'main_form_list': main_form_list,
            'main_form': main_form,
            'main_form_id': main_form_id,
            'result_line_list': result_line_list,
        }
        return render(request, TEMPLATE_PATH + 'result_form.html', context)
    
    elif request.method == 'POST':
        def save_result_line(request, result):
            count_result_line = int(request.POST.get('count_result_line', None))
            for i in range(0, count_result_line):
                try:
                    result_line = ResultLine.objects.get(pk=int(request.POST.get('result_line_list['+str(i)+'].id', None)))
                except:
                    result_line = ResultLine()
                try:
                    outcome = Outcome.objects.get(pk=int(request.POST.get('result_line_list['+str(i)+'].outcome', None)))
                except:
                    outcome = None
                try:
                    feedback = Feedback.objects.get(pk=int(request.POST.get('result_line_list['+str(i)+'].feedback', None)))
                except:
                    feedback = None
                result_line.outcome = outcome
                result_line.feedback = feedback
                result_line.result = result
                result_line.save()

        form = ResultForm(request.POST)
        if form.is_valid():
            result = form.save(commit=False)
            if request.POST.get("id", "") != '':
                result_temp = Result.objects.get(pk=int(request.POST.get("id", "")))
                result.id = result_temp.pk
                result.create_at = result_temp.create_at
                result.update_at = datetime.datetime.now()
            result.user = request.user
            result.save()

            save_result_line(request, result)
        else:
            print(form.errors)

        return HttpResponseRedirect(reverse('form:result_list'))


def result_update(request, pk):
    if request.method == 'GET':
        main_form_list = MainForm.objects.all()
        result = Result.objects.get(pk=pk)
        result_line_list = []
        for result_line in ResultLine.objects.filter(result=result):
            feedback_list = Feedback.objects.filter(outcome=result_line.outcome)
            outcome_feedback = {
                'result_line': result_line,
                'feedback_list': feedback_list,
            }
            result_line_list.append(outcome_feedback)
        main_form_id = result.main_form.pk
        context = {
            'main_form_list': main_form_list,
            'main_form_id': main_form_id,
            'result': result,
            'result_line_list': result_line_list,
        }
        return render(request, TEMPLATE_PATH + 'result_form.html', context)


def result_delete(request, pk):
    Result.objects.filter(pk=pk).delete()
    return HttpResponseRedirect(reverse('form:result_list'))