from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from crispy_forms.bootstrap import Tab, TabHolder, InlineCheckboxes, InlineRadios
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Row, Column, Field, Layout
from django.db import models
from django.db import transaction


class SignUpForm(UserCreationForm):
          
    ROLE_CHOICES = (
        (1, 'Student'),
        (2, 'Teacher'),
        
    )

    username = forms.CharField(max_length=20)
    email = forms.EmailField(max_length=20)
    password1 = forms.CharField(max_length=20, label='Password', widget=forms.PasswordInput())
    password2 = forms.CharField(max_length=20, label='Confirm Password', widget=forms.PasswordInput())
    user_role = forms.ChoiceField(choices=ROLE_CHOICES, widget=forms.Select())

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].required = True
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
                
                    Row(
                        Field('username', css_class='col-md-3'),
                        Field('email', css_class='col-md-3'),
                        Field('password1', css_class='col-md-3'),
                        Field('password2', css_class='col-md-3'),
                        Field('user_role', css_class='col-md-3')
                    ),
        )

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.user_role = True
        user.save()

        return user